# codefirst-pastebin

```bash
    cd /tmp/
    git clone https://github.com/w4/bin.git
    cd bin
    docker build -t hub.codefirst.iut.uca.fr/thomas.bellembois/codefirst-pastebin:latest .
    docker push hub.codefirst.iut.uca.fr/thomas.bellembois/codefirst-pastebin:latest
```